#!/usr/bin/env bash
set -e

#echo "REGIONS"
#echo "serial"
#bgsignature count -r ~/Desktop/cds.regions.gz|sort>cds1
#echo "parallel"
#bgsignature count -r ~/Desktop/cds.regions.gz --cores 4 |sort>cds2
#diff cds1 cds2

#echo "REGIONS with GROUPS"
#echo "serial"
#bgsignature count -r ~/Desktop/cds.regions.gz --group SYMBOL |sort>cds3
#echo "parallel"
#bgsignature count -r ~/Desktop/cds.regions.gz --group SYMBOL --cores 4 |sort>cds4
#diff cds3 cds4
#
#echo "MUTATIONS"
#echo "serial"
#bgsignature count -m ~/Desktop/paad.txt |sort>paad1
#echo "parallel"
#bgsignature count -m ~/Desktop/paad.txt --cores 4 |sort>paad2
#diff paad1 paad2


echo "MUTATIONS with GROUPS"
echo "serial"
bgsignature count -m ~/Desktop/paad.txt --group SAMPLE |sort>paad3
echo "parallel"
bgsignature count -m ~/Desktop/paad.txt --group SAMPLE --cores 4 |sort>paad4
diff paad3 paad4